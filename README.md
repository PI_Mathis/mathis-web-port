# Mathis' Web Portfolio

[![Build Status](https://travis-ci.org/mathisGarberg/mathis-web-port.svg?branch=master)](https://travis-ci.org/mathisGarberg/mathis-web-port)
[![Documentation Status](https://readthedocs.org/projects/mathis-web-port/badge/?version=latest)](https://mathis-web-port.readthedocs.io/en/latest/?badge=latest)

## Documentation
View documentation [here](https://mathis-web-port.readthedocs.io/en/latest/) 

Hello and welcome to the main repository for my own portfolio page.

![themes](https://raw.githubusercontent.com/mathisGarberg/mathis-web-port/master/meta-assets/about.PNG)
