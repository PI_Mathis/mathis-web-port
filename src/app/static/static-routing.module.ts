import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutComponent } from './about/about.component';
import { ProjectsComponent } from './projects/projects.component';
import { ProjectComponent } from './project/project.component';
import { StoriesComponent } from './stories/stories.component';
import { ContactComponent } from './contact/contact.component';

const routes: Routes = [
  {
    path: 'about',
    component: AboutComponent,
    data: { title: 'app.menu.about' }
  },
  {
    path: 'projects/:tag',
    component: ProjectsComponent,
    data: { title: 'app.menu.projects' }
  },
  {
    path: 'project/:tag/:id',
    component: ProjectComponent,
    data: { title: 'app.menu.project' }
  },
  {
    path: 'stories',
    component: StoriesComponent,
    data: { title: 'app.menu.stories' }
  },
  {
    path: 'contact',
    component: ContactComponent,
    data: { title: 'app.menu.contact' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaticRoutingModule {}
