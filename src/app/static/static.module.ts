import { NgModule } from '@angular/core';
import { LayoutModule } from '@angular/cdk/layout';

import { SharedModule } from '@app/shared';

import { StaticRoutingModule } from './static-routing.module';
import { AboutComponent } from './about/about.component';
import { ProjectsComponent } from './projects/projects.component';
import { ProjectComponent } from './project/project.component';
import { StoriesComponent } from './stories/stories.component';
import { ContactComponent } from './contact/contact.component';

import {
  CarouselComponent,
  CarouselItemElement
} from './carousel/carousel.component';
import { CarouselItemDirective } from './carousel-item.directive';

@NgModule({
  imports: [LayoutModule, SharedModule, StaticRoutingModule],
  declarations: [
    AboutComponent,
    ProjectsComponent,
    ProjectComponent,
    StoriesComponent,
    ContactComponent,

    CarouselComponent,
    CarouselItemDirective,
    CarouselItemElement
  ]
})
export class StaticModule {}
