import browser from 'browser-detect';
import {
  AfterViewInit,
  Component,
  ContentChildren,
  Directive,
  ElementRef,
  Input,
  Renderer2,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { CarouselItemDirective } from '../carousel-item.directive';
import {
  animate,
  AnimationBuilder,
  AnimationFactory,
  AnimationPlayer,
  style
} from '@angular/animations';
import { BreakpointObserver } from '@angular/cdk/layout';

@Directive({
  selector: '.carousel-item'
})
export class CarouselItemElement {}

export interface HasElementRef {
  _elementRef: ElementRef;
}

@Component({
  selector: 'carousel',
  exportAs: 'carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements AfterViewInit {
  @ContentChildren(CarouselItemDirective, { read: false }) items: QueryList<
    CarouselItemDirective
  >;
  @ViewChildren(CarouselItemElement, { read: ElementRef })
  private itemsElements: QueryList<ElementRef>;
  @ViewChild('carousel', { static: false }) private carousel: ElementRef;
  @Input() timing = '300ms ease-in';
  @Input() showControls = true;
  @Input() totalItems: number;
  private player: AnimationPlayer;
  private itemWidth: number;
  private currentSlide = 0;
  carouselWrapperStyle = {};

  constructor(
    private builder: AnimationBuilder,
    public breakpointObserver: BreakpointObserver,
    private renderer: Renderer2
  ) {}

  ngAfterViewInit() {
    this.itemsElements.changes.subscribe(itemsElements => {
      let w: string = itemsElements.length * 100 + '%';

      this.renderer.setStyle(this.carousel.nativeElement, 'width', w);

      let slideWidthInContainer: string = 100 / itemsElements.length + '%';

      this.itemWidth = 100 / itemsElements.length;

      itemsElements.forEach(el => {
        this.renderer.setStyle(
          el.nativeElement,
          'width',
          slideWidthInContainer
        );
      });

      this.carouselWrapperStyle = {
        width: `100%`
      };
    });
  }

  next() {
    if (this.currentSlide + 1 === this.items.length) return;
    this.currentSlide = (this.currentSlide + 1) % this.items.length;
    const offset = this.currentSlide * this.itemWidth;

    if (!CarouselComponent.isIEorEdgeOrSafari()) {
      const myAnimation: AnimationFactory = this.buildAnimation(offset);
      this.player = myAnimation.create(this.carousel.nativeElement);
      this.player.play();
    } else {
      this.renderer.setStyle(
        this.carousel.nativeElement,
        'transform',
        `translateX(-${offset}%)`
      );
    }
  }

  prev() {
    if (this.currentSlide === 0) return;

    this.currentSlide =
      (this.currentSlide - 1 + this.items.length) % this.items.length;
    const offset = this.currentSlide * this.itemWidth;

    if (!CarouselComponent.isIEorEdgeOrSafari()) {
      const myAnimation: AnimationFactory = this.buildAnimation(offset);
      this.player = myAnimation.create(this.carousel.nativeElement);
      this.player.play();
    } else {
      this.renderer.setStyle(
        this.carousel.nativeElement,
        'transform',
        `translateX(-${offset}%)`
      );
    }
  }

  private buildAnimation(offset) {
    return this.builder.build([
      animate(this.timing, style({ transform: `translateX(-${offset}%)` }))
    ]);
  }

  private static isIEorEdgeOrSafari() {
    return ['ie', 'edge', 'safari'].includes(browser().name);
  }
}
