import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import { environment as env } from '@env/environment';
import { ROUTE_ANIMATIONS_ELEMENTS, StoryService, Story } from '@app/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StoriesComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  versions = env.versions;

  stories$: Observable<Story[]>;

  constructor(private storyService: StoryService) {}

  ngOnInit() {
    this.loadStories();
  }

  loadStories() {
    this.stories$ = this.storyService.getAll().pipe(map(story => story));
  }

  showSummary(description) {
    if (!description) return description;
    return `${description
      .split(' ')
      .slice(0, 20)
      .join(' ')} ...`;
  }
}
