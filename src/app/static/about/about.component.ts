import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Meta } from '@angular/platform-browser';

import {
  ROUTE_ANIMATIONS_ELEMENTS,
  SkillService,
  SEOService,
  Skill
} from '@app/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AboutComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  myName = 'Mathis Garberg';
  skills$: Observable<Skill[]>;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private meta: Meta,
    private skillService: SkillService,
    private seoService: SEOService
  ) {
    this.meta.addTags([
      { name: 'description', content: 'My personal portfolio' },
      { name: 'author', content: 'Mathis Garberg' },
      { name: 'keywords', content: 'Mathis Portfolio' }
    ]);
  }

  ngOnInit() {
    this.loadSkills();
  }

  loadSkills() {
    this.skills$ = this.skillService.getAll().pipe(map(skill => skill));
  }
}
