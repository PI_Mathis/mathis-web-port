import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { environment as env } from '@env/environment';
import { ROUTE_ANIMATIONS_ELEMENTS, ProjectService, Project } from '@app/core';
import { map, finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  versions = env.versions;
  animationState: string;
  isLoading: boolean;

  projects$: Observable<Project[]>;

  constructor(
    private projectService: ProjectService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.loadProjects(this.route.snapshot.paramMap.get('tag'));
  }

  loadProjects(category: string) {
    this.isLoading = true;

    this.projects$ = this.projectService.getByCategory(category).pipe(
      map(res => {
        const currentItem = res.filter(
          value => value._id === this.route.snapshot.paramMap.get('id')
        );
        return this.immutableMove(res, res.indexOf(currentItem[0]), 0);
      }),
      finalize(() => (this.isLoading = false)),
      map(projects => {
        return projects;
      })
    );
  }

  private immutableMove(arr, from, to) {
    return arr.reduce((prev, current, idx, self) => {
      if (from === to) {
        prev.push(current);
      }
      if (idx === from) {
        return prev;
      }
      if (from < to) {
        prev.push(current);
      }
      if (idx === to) {
        prev.push(self[from]);
      }
      if (from > to) {
        prev.push(current);
      }
      return prev;
    }, []);
  }
}
