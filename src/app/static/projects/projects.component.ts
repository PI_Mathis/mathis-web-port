import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, finalize, takeUntil, mergeMap } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import {
  trigger,
  transition,
  query,
  style,
  stagger,
  animate
} from '@angular/animations';

import { environment as env } from '@env/environment';
import {
  ROUTE_ANIMATIONS_ELEMENTS,
  ProjectService,
  SectionService,
  Project
} from '@app/core';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss'],
  animations: [
    trigger('listStagger', [
      transition('* <=> *', [
        query(
          ':enter',
          [
            style({ opacity: 0, transform: 'translateY(-15px)' }),
            stagger(75, [
              style({ transform: 'translateY(10%)', opacity: 0 }),
              animate(
                '0.5s ease-in-out',
                style({ transform: 'translateY(0%)', opacity: 1 })
              )
            ])
          ],
          { optional: true }
        ),
        query(':leave', animate('50ms', style({ opacity: 0 })), {
          optional: true
        })
      ])
    ])
  ]
})
export class ProjectsComponent implements OnInit, OnDestroy {
  private unsubscribe$: Subject<void> = new Subject<void>();

  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  versions = env.versions;

  isLoading: boolean;
  projects$: Observable<Project[]>;
  sections$: Observable<any>;

  constructor(
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private sectionService: SectionService
  ) {}

  ngOnInit() {
    this.loadProjects(this.route.snapshot.paramMap.get('tag'));
    this.loadSections();
    this.loadTotalProjectsPerCategory();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.complete();
    this.unsubscribe$.unsubscribe();
  }

  loadProjects(category: string) {
    this.isLoading = true;

    this.projects$ = this.projectService.getByCategory(category).pipe(
      finalize(() => (this.isLoading = false)),
      takeUntil(this.unsubscribe$),
      map(projects => {
        return projects;
      })
    );
  }

  loadTotalProjectsPerCategory() {
    this.sectionService
      .getLinks()
      .pipe(
        map(sections => sections),
        mergeMap(() => this.projectService.getTotalProjectsPerCategory())
      )
      .subscribe(res => console.log(res));
  }

  onCategoryChange(category: string) {
    this.loadProjects(category);
  }

  loadSections() {
    this.sections$ = this.sectionService
      .getLinks()
      .pipe(map(sections => sections));
  }

  openLink(link: string) {
    window.open(link, '_blank');
  }
}
