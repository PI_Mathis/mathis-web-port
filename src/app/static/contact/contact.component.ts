import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';

import {
  ROUTE_ANIMATIONS_ELEMENTS,
  NotificationService,
  MailService
} from '@app/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;

  form = this.fb.group({
    subject: ['', [Validators.required]],
    description: [
      '',
      [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(1000)
      ]
    ]
  });

  constructor(
    private fb: FormBuilder,
    private notificationService: NotificationService,
    private mailService: MailService
  ) {}

  ngOnInit() {}

  submit() {
    this.mailService
      .sendMail(this.form.value)
      .subscribe(res => console.log(res));

    this.notificationService.success('Success message');
  }

  reset() {
    this.form.reset();
    this.form.clearValidators();
    this.form.clearAsyncValidators();
  }
}
