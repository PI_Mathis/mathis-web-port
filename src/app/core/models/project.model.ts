export class Project {
  _id: string;
  title: string;
  sections: any[];
  year: Date | number;
  tags: Array<string>;
  roles: Array<string>;
  paragraphs: Array<string>;
  repoLink: string;
  technologies: Array<any>;
  codeSandboxSrc: string;
  thumbnail: string;
  prevId: string;
  nextId: string;
}
