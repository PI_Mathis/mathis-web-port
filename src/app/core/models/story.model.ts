export class Story {
    id: number;
    name: string;
    title: string;
    description: string;
    imageUrl: string;
    link: string;
}
