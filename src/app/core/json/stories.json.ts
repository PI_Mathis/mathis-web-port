export const stories = [
  {
    id: 1,
    name: 'app.story.folder-structure.long',
    title: 'app.story.folder-structure.short',
    description: 'app.story.folder-structure.desc',
    imageUrl: '',
    link:
      'https://itnext.io/choosing-a-highly-scalable-folder-structure-in-angular-d987de65ec7'
  },
  {
    id: 2,
    name: 'app.story.modal-forms.long',
    title: 'app.story.modal-forms.short',
    description: 'app.story.modal-forms.desc',
    imageUrl: '',
    link:
      'https://itnext.io/creating-forms-inside-modals-with-ng-bootstrap-221e4f1f5648'
  },
  {
    id: 3,
    name: 'app.story.ngx-rocket.long',
    title: 'app.story.ngx-rocket.short',
    description: 'app.story.ngx-rocket.desc',
    imageUrl: '',
    link:
      'https://itnext.io/creating-enterprise-angular-apps-with-the-ngx-rocket-generator-871ac76ace87'
  },
  {
    id: 4,
    name: 'app.story.clean-code.long',
    title: 'app.story.clean-code.short',
    description: 'app.story.clean-code.desc',
    imageUrl: '',
    link:
      'https://itnext.io/clean-code-checklist-in-angular-%EF%B8%8F-10d4db877f74'
  }
];
