import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Mail } from '../models/mail.model';

@Injectable({
  providedIn: 'root'
})
export class MailService {
  constructor(private httpClient: HttpClient) {}

  sendMail(payload: Mail): Observable<Mail> {
    return this.httpClient.post('/api/send-mail', payload).pipe(
      map((body: any) => body),
      catchError(() => of('Error, could not load data'))
    );
  }
}
