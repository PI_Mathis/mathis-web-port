import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Project } from '../models/project.model';
import { projects } from '../json/projects.json';

let counter = 0;

@Injectable()
export class ProjectService {

    private projectArray: any[] = projects;

    constructor() { }
  
    getSingle(id: number): Observable<Project> {
      counter = (counter + 1) % this.projectArray.length;
      return of(this.projectArray[counter]);
    }

}