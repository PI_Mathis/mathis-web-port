import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

let counter = 0;

@Injectable()
export class SectionService {
  private sections = [
    {
      title: 'Tags',
      links: [
        {
          id: 1,
          name: 'games',
          link: 'games',
          active: false
        },
        {
          id: 2,
          name: 'web development',
          link: 'web-development',
          active: true
        },
        {
          id: 3,
          name: 'angular',
          active: false
        },
        {
          id: 4,
          name: 'javascript',
          active: false
        },
        {
          id: 5,
          name: 'material design',
          active: false
        },
        {
          id: 6,
          name: 'ionic',
          active: false
        },
        {
          id: 7,
          name: 'vue',
          active: false
        },
        {
          id: 8,
          name: 'react',
          active: false
        },
        {
          id: 9,
          name: 'telenor',
          active: false
        },
        {
          id: 10,
          name: 'rxjs',
          active: false
        },
        {
          id: 11,
          name: 'ngrx',
          active: false
        },
        {
          id: 12,
          name: 'mongodb',
          active: false
        },
        {
          id: 13,
          name: 'node',
          active: false
        },
        {
          id: 14,
          name: 'ubuntu',
          active: false
        }
      ]
    }
  ];

  private sectionArray: any[];

  getLinks(): Observable<any> {
    return of(this.sections);
  }

  getLinkArray(): Observable<any[]> {
    return of(this.sectionArray);
  }

  getLink(): Observable<any> {
    counter = (counter + 1) % this.sectionArray.length;
    return of(this.sectionArray[counter]);
  }
}
