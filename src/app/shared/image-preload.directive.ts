import { Directive, Input, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: 'img[default]',
  host: {
    '[src]': 'src'
  }
})
export class ImagePreloadDirective {
  @Input() src: string;
  @Input() default: string;
  @HostBinding('class') className;

  @HostListener('error') updateUrl() {
    this.src = this.default;
  }

  @HostListener('load') load() {
    this.className = 'image-loaded';
  }
}
