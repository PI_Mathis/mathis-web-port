import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { ScrollDispatcher, CdkScrollable } from '@angular/cdk/overlay';

@Component({
  selector: 'app-scroll-to-top',
  templateUrl: './scroll-to-top.component.html',
  styleUrls: ['./scroll-to-top.component.scss']
})
export class ScrollToTopComponent implements AfterViewInit {
  navIsFixed: boolean;

  @ViewChild(CdkScrollable, { static: false }) scrollable: CdkScrollable;

  constructor(private scroll: ScrollDispatcher) {
    this.scroll.scrolled().subscribe((data: CdkScrollable) => {
      this.onWindowScroll(data);
    });
  }

  ngAfterViewInit() {
    // console.log(this.scrollable);
  }

  scrollToTop() {
    // console.log(this.scrollable);
  }

  onWindowScroll(data: CdkScrollable) {
    const scrollTop = data.getElementRef().nativeElement.scrollTop || 0;
    if (scrollTop > 100) {
      this.navIsFixed = true;
      // console.log('Over 100', this.navIsFixed);
    } else if (scrollTop < 10) {
      this.navIsFixed = false;
      // console.log('Under 10', this.navIsFixed);
    }
  }
}
