import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeedDialButtonsComponent } from './speed-dial-buttons.component';

describe('SpeedDialButtonsComponent', () => {
  let component: SpeedDialButtonsComponent;
  let fixture: ComponentFixture<SpeedDialButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpeedDialButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpeedDialButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
