import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTE_ANIMATIONS_ELEMENTS } from '@app/core';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss']
})
export class SectionComponent implements OnInit {
  @Input() section: any;
  @Output() categoryChange = new EventEmitter<string>();
  hiddenItems: any[];
  displayedItems: any[];
  isCollapsed = true;
  collapseText = this.isCollapsed ? 'app.show-all' : 'app.hide';
  selectedLink = 1;
  displayNum = 10;
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;

  constructor(private router: Router) {}

  ngOnInit() {
    if (this.section.links.length >= 2) {
      this.displayedItems = this.createDisplayedArray(this.section.links);
      this.hiddenItems = this.createHiddenArray(this.section.links);
    } else {
      this.displayedItems = this.section.links;
    }
  }

  createDisplayedArray(items: any) {
    return items.slice(0, this.displayNum);
  }

  createHiddenArray(items: any) {
    return items.slice(2, this.section.links.length);
  }

  toggleContent() {
    this.isCollapsed = !this.isCollapsed;
    this.collapseText = this.isCollapsed ? 'app.show-all' : 'app.hide';
  }

  onCategoryChange(category: string, index: number) {
    console.log(index);
    this.selectedLink = index;
    this.categoryChange.emit(category);
  }
}
