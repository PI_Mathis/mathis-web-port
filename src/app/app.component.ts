import browser from 'browser-detect';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  NavigationCancel,
  Event,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router
} from '@angular/router';
import { CdkScrollable, ScrollDispatcher } from '@angular/cdk/overlay';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';

import { routeAnimations, fadeAnimations, AppState } from '@app/core';
import { environment as env } from '@env/environment';

import {
  ActionSettingsChangeLanguage,
  ActionSettingsChangeAnimationsPageDisabled,
  selectEffectiveTheme,
  selectSettingsLanguage,
  selectSettingsStickyHeader
} from './settings';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [routeAnimations, fadeAnimations]
})
export class AppComponent implements OnInit {
  @ViewChild(CdkScrollable, { static: false }) scrollable: CdkScrollable;

  lastOffset = 0;
  isScrollToTop = false;
  isProd = env.production;
  envName = env.envName;
  version = env.versions.app;
  logo = require('../assets/images/logo-resized.png');
  languages = ['en', 'no'];
  year = new Date().getFullYear();
  navigation = [
    { link: 'about', label: 'app.menu.about' },
    { link: 'projects/web-development', label: 'app.menu.projects' },
    { link: 'stories', label: 'app.menu.stories' },
    { link: 'contact', label: 'app.menu.contact' }
    // { link: 'admin', label: 'app.menu.admin' }
  ];
  navigationSideMenu = [
    ...this.navigation,
    { link: 'settings', label: 'app.menu.settings' }
  ];

  stickyHeader$: Observable<boolean>;
  language$: Observable<string>;
  theme$: Observable<string>;

  scrollingSubscription = new Subscription();

  constructor(
    private router: Router,
    private store: Store<AppState>,
    private scroll: ScrollDispatcher
  ) {
    this.router.events.subscribe((event: Event) => {
      this.navigationInterceptor(event);
    });

    this.scrollingSubscription = this.scroll
      .scrolled()
      .subscribe((data: CdkScrollable) => {
        this.onWindowScroll(data);
      });
  }

  private static isIEorEdgeOrSafari() {
    return ['ie', 'edge', 'safari'].includes(browser().name);
  }

  ngOnInit(): void {
    if (AppComponent.isIEorEdgeOrSafari()) {
      this.store.dispatch(
        new ActionSettingsChangeAnimationsPageDisabled({
          pageAnimationsDisabled: true
        })
      );
    }

    this.stickyHeader$ = this.store.pipe(select(selectSettingsStickyHeader));
    this.language$ = this.store.pipe(select(selectSettingsLanguage));
    this.theme$ = this.store.pipe(select(selectEffectiveTheme));
  }

  private navigationInterceptor(event: Event): void {
    this.scrollable.getElementRef().nativeElement.scrollTo({ top: 0 });
  }

  private onWindowScroll(data: CdkScrollable) {
    const scrollTop = data.getElementRef().nativeElement.scrollTop || 0;
    if (this.lastOffset > scrollTop) {
      this.isScrollToTop = false;
    } else if (scrollTop < 10) {
      this.isScrollToTop = true;
    } else if (scrollTop > 100) {
      this.isScrollToTop = true;
    }

    this.lastOffset = scrollTop;
  }

  onScrollToTop() {
    this.scrollable
      .getElementRef()
      .nativeElement.scrollTo({ top: 0, behavior: 'smooth' });
  }

  onLanguageSelect({ value: language }) {
    this.store.dispatch(new ActionSettingsChangeLanguage({ language }));
  }
}
