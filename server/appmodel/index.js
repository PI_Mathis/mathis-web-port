global.logger = require('../helpers/logger');

const {resolve} = require('path');

const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');

class AppModel {
  constructor(NAME) {
    this.NAME = NAME;

    this.appimage = express();
    this.appimage.use(bodyParser.json());
    this.appimage.use(express.static(path.join(__dirname, '../../dist')));
    this.appimage.use('/', express.static(path.join(__dirname, '../../dist')));
  }

  withSwagger(docName) {
    const swaggerUi = require('swagger-ui-express');
    const swaggerDocument = require(docName); 

    this.appimage.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

    return this;
  }

  build() {
    return this.appimage;
  }
}

module.exports = AppModel;