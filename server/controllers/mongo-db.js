const api = require('../api');
const config = require('../config/envconfig');
const Project = require('../schemas/projects.schema');

module.exports = {

  // find all projects
  projects(req, res) {
    Project.find({}, (err, projects) => {
      if (err) {
        console.log(err);
      } else {
        res.send(projects);
      }
    })
  },

  // find a single project
  async project(req, res) {
    const {id} = req.params;

    try {
      const project = await Project.find({_id: id});
      res.send(project);
    } catch(err) {
      next(err);
    }
  },

  // find projects by category name
  async projectsByCategory(req, res) {
    const {id} = req.params;

    try {
      const categories = await Project.find({categories: id});
      res.send(categories)
    } catch(err) {
      next(err);
    }
  },

  // find projects by tag name
  async projectByTag(req, res) {
    const {id} = req.params;

    try {
      const tags = await Project.find({tags: id});
      res.send(tags);
    } catch(err) {
      next(err);
    }
  },

  async totalProjectsPerCategory(req, res) {
    try {
      const totalProjectsPerCategory = await Project.aggregate([
        { $unwind: '$categories' },
        {
          $group: {
          _id: '$categories',
          count: {$sum: 1}
        }
      }
     ]);
      res.send(totalProjectsPerCategory);
    } catch(err) {
      next(err);
    }
  }
};