module.exports = (prepath, app) => {
  app.use(prepath, require('../routes/projects'));
  app.use(require('../middleware/error'));
};