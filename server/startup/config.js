const bodyParser = require('body-parser');
const cors = require('cors');
const config = require('../config/DB');
const mongoose = require('mongoose');

module.exports = (app) => {
  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: false}));

  mongoose.connect(config.DB, { useNewUrlParser: true })
    .then(() => console.log('connection was succesful'))
    .catch((err) => console.error(err));
  mongoose.Promise = global.Promise;
};