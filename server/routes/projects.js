const express = require('express');
const router = express.Router();
const mongo_db = require('../controllers/mongo-db');
const asyncMiddleware = require('../middleware/asyncMiddleware');
const checkMissingHeaders = require('../middleware/checkMissingHeaders');

router.get('/projects', asyncMiddleware(mongo_db.projects));
router.get('/projects/:id', asyncMiddleware(mongo_db.project));
router.get('/categories/:id', asyncMiddleware(mongo_db.projectsByCategory));
router.get('/tags/:id', asyncMiddleware(mongo_db.projectByTag));
router.get('/totalCategories', asyncMiddleware(mongo_db.totalProjectsPerCategory));

module.exports = router;
