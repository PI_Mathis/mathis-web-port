global.logger = require('../helpers/logger');

const config = require('../config/envconfig');
const app = require('../app').app('CLIENT', '/api/v1');
const http = require('http');

const CLIENT = app.listen(config.port, () => {
    logger.info(`CLIENT started on port ${config.port}`)
});