const config = require('./config/envconfig');
const axios = require('axios');

axios.defaults.headers.common = {
  "Content-type": "application/json"
};

// const username = config.resourceOne.user;
// const password = config.resourceOne.password;
// const token = `Basic ${Buffer.from(`${username}:${password}`).toString('base64')}`;

// module.exports = axios.create({
//     baseUrl: config.resourceOne.host,
//     headers: {'Authorization': token}
// });
