const {resolve} = require('path');
const AppModel = require('./appmodel');

module.exports = {
    app: (name, prepath = '') => {
        const app = new AppModel(name)
            .withSwagger('../doc/swagger.json')
            .build();

        require('./startup/config')(app);
        require('./startup/routes')(`${prepath}`, app);

        return app;
    }
};