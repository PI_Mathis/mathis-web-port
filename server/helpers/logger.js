const winston = require('winston');
const { combine, timestamp, printf } = winston.format;

const myFormat = printf(info => {
    return `${info.timestamp} - [${info.level}]: message=${info.message}`;
});

const loggingLevels = {
    audit: 0,
    warn: 1,
    error: 2,
    info: 3
};

const logger = winston.createLogger({
    transports: [new winston.transports.Console()],
    levels: loggingLevels,
    format: combine(
        timestamp(),
        myFormat
    ),
    timestamp: true
})

module.exports = logger;