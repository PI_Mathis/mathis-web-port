module.exports = {
  port: process.env.PORT || 3000,
  mongo_db: {
    host: (user, password) => process.env.host || `mongodb://${user}:${password}ds141358.mlab.com:41358/ngx-portolfio`,
    endpoints: {
      projects: '/projects',
      project: '/projects/:id',
      categories: '/projects/category',
      category: '/projects/category/:category'
    }
  }
};

