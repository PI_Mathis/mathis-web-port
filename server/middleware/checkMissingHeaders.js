module.exports = (...keys) => {
  return (req, res, next) => {
    const hasHeaders = keys
      .map(key => !!req.headers[key.toLowerCase()])
      .indexOf(false) === -1;

    if (!hasHeaders) {
      return res.status(400).json({
        message: `Make sure all headers: [${keys}] are included before trying again.`
      });
    }
    next();
  }
}